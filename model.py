from config import *

class Filmes(db.Entity):
    titulo = Required(str)
    diretor = Required(str)
    ano_de_lançamento = Optional(float)
    categoria = Required(str)
    faixa_etaria = Optional (float)

    def __str__(self):
        return f'{self.titulo}, {self.diretor}, {self.ano_de_lançamento}, {self.categoria}, {self.faixa_etaria}'

db.bind(provider='sqlite', filename='person.db', create_db=True)
db.generate_mapping(create_tables=True)
