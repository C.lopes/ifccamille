from config import *
from model import *

@app.route("/")
def home():
    return render_template("home.html")

@app.route("/about")
def about():
    return render_template("about.html")

@app.route("/listar_filmes")
def listar_filmes():
    with db_session:
        # obtém os filmes 
        filmes = Filmes.select() 
        return render_template("listar_filmes.html", filmes=filmes)

@app.route("/form_adicionar_filmes")
def form_adicionar_filmes():
    return render_template("form_adicionar_filmes.html")

@app.route("/adicionar_filme")
def adicionar_filmes():
    # obter os parâmetros
    titulo = request.args.get("titulo")
    diretor = request.args.get("diretor")
    ano_de_lançamento = request.args.get("ano_de_lançamento")
    categoria = request.args.get("categoria")
    faixa_etaria = request.args.get("faixa_etaria")

    # salvar
    with db_session:
        # criar a filme
        f = Filmes(**request.args)
        # salvar
        commit()
        # encaminhar de volta para a listagem
        return redirect("listar_filmes") 

'''
run:
$ flask run
'''
